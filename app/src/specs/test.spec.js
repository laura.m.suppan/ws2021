function adder(x, y) {
    return x + y;
}

test('can add two value', () => {
    expect(adder(1, 2)).toBe(3)
})